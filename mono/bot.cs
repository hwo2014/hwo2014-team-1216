using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Bot {

	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));
		}

        Console.ReadKey();
	}

	private StreamWriter writer;


	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);


        TrackWrapper track = null;
        double lastInPieceDistance = 0;
        double lastSpeed = 0;
        int lastPieceIndex = 0;
        double lastThrottle = 0;
        bool haveTurbo = false;
        bool wasTurbo = false;

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
                    var pos = JsonConvert.DeserializeObject<PositionsWrapper>(line);
                    var mypos = pos.data[0];
                    var currentPiece = track.pieces[mypos.piecePosition.pieceIndex];

                    double additionalDistance = 0;
                    if (lastPieceIndex != mypos.piecePosition.pieceIndex)
                    {
                        additionalDistance = track.GetPieceRealLength(lastPieceIndex, mypos.piecePosition.lane.startLaneIndex) - lastInPieceDistance;  //TODO: check here on lanes change
                        lastInPieceDistance = 0;
                        lastPieceIndex = mypos.piecePosition.pieceIndex;
                    }

                    var currentSpeed = mypos.piecePosition.inPieceDistance - lastInPieceDistance + additionalDistance;

                    lastInPieceDistance = mypos.piecePosition.inPieceDistance;

                    //TODO: dirty fix for get real distance

                    var accelleration = currentSpeed - lastSpeed;
                    lastSpeed = currentSpeed;
                   
                    var nextPieceIndex = (mypos.piecePosition.pieceIndex + 1) % track.pieces.Length;
                    var currentPieceIndex = mypos.piecePosition.pieceIndex;
                    var nextPiece = track.pieces[nextPieceIndex];
                    var currentLaneIndex = mypos.piecePosition.lane.startLaneIndex; //TODO: check here on lanes change


                    var s = 1.0;
                    var f =  0.465;
                    var a = currentSpeed / 35;


                    var distanceToAngle = -mypos.piecePosition.inPieceDistance;
                    var anglePieceIndex = currentPieceIndex;

                    while(true)
                    {
                        distanceToAngle += track.GetPieceRealLength(anglePieceIndex, currentLaneIndex);
                        anglePieceIndex = (anglePieceIndex + 1) % track.pieces.Length;
                        if (track.pieces[anglePieceIndex].IsAngle())
                            break;
                    }


                    var nextPieceIsAngle = nextPiece.IsAngle();
                    var nextAngleMaxSpeed = Math.Sqrt(track.GetPieceRealRadius(anglePieceIndex, currentLaneIndex) * f);
                    var thisPieceIsAngle = currentPiece.IsAngle();
                    var thisPieceMaxSpeed = Math.Sqrt(track.GetPieceRealRadius(currentPieceIndex, currentLaneIndex) * f);

                    var breakDistance = (currentSpeed * currentSpeed - nextAngleMaxSpeed * nextAngleMaxSpeed) / (2 * a);
                    var needToBreak = distanceToAngle + currentSpeed < breakDistance; //TODO: BACK HERE!
                    var isAfterHalf = track.GetPieceRealLength(currentPieceIndex, currentLaneIndex) / 2 < mypos.piecePosition.inPieceDistance;
                    var isAfterSafePoint = track.GetPieceRealLength(currentPieceIndex, currentLaneIndex) / 1.95 < mypos.piecePosition.inPieceDistance;
                    var nextPieceIsNotTheSameAngle = Math.Sign(currentPiece.angle) != Math.Sign(nextPiece.angle);


                    if (!wasTurbo && thisPieceIsAngle && currentSpeed > thisPieceMaxSpeed)
                    {
                        s = 0.0;
                    }


                    if (needToBreak && currentSpeed > nextAngleMaxSpeed)
                    {
                        s = 0.0;
                        wasTurbo = false;
                    }


                    if (currentSpeed > 10 && distanceToAngle < 465)
                        s = 0.0;

                    //if(nextPieceIsAngle)
                    //    Console.WriteLine(mypos.piecePosition.pieceIndex + " -> " + s.ToString("0.00") + " -> " + distanceToAngle.ToString("0.00"));

                    //if (nextPieceIsAngle && pieceAfterNextIsSameAngle && distance > 3)
                    //{
                    //    Console.WriteLine("T1 - " + mypos.piecePosition.pieceIndex);
                    //    s = 0.2;
                    //}

                    //if (nextPieceIsAngle && isAfterHalf && distance > 3)
                    //{
                    //    Console.WriteLine("T2 - " + mypos.piecePosition.pieceIndex);
                    //    s = 0.1;
                    //}

                    var canUseTurbo = isAfterSafePoint && nextPiece.angle == 0 && track.pieces[(currentPieceIndex + 2) % track.pieces.Length].angle == 0 && track.pieces[(currentPieceIndex + 3) % track.pieces.Length].angle == 0;

                    if (haveTurbo && canUseTurbo)
                    {
                        Console.WriteLine("KURRRRWAAAAAAAA!");
                        var command = "{\"msgType\": \"turbo\", \"data\": \"KURWAAAAAA!!!!\"}";
                        writer.WriteLine(command);
                        haveTurbo = false;
                        wasTurbo = true;
                    }


                    lastThrottle = s;
                    send(new Throttle(s));
                    


					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine("Race init");
                    RaceInitWrapper game = JsonConvert.DeserializeObject<RaceInitWrapper>(line);
                    track = game.data.race.track;
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
                case "turboAvailable":
                    haveTurbo = true;
                    Console.WriteLine("GOT TURBO!");
                    break;
				default:
                    Console.WriteLine(msg.msgType);
                    Console.WriteLine(msg.data);
                    Console.Beep(200, 100);
					send(new Ping());
					break;
			}
		}
	}

    private void laneSwitch(string direction) {
        var command = "{\"msgType\": \"switchLane\", \"data\": \"" + direction + "\"}";
        writer.WriteLine(command);
    }

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

class CarIdWrapper {
    public string name;

}

class PiecePositionLaneWrapper
{
    public int startLaneIndex;
    public int endLaneIndex;
}

class PiecePositionsWrapper
{
    public int pieceIndex;
    public double inPieceDistance;
    public PiecePositionLaneWrapper lane;
    public int lap;
}

class CarWrapper
{
    public CarIdWrapper id;
    public PiecePositionsWrapper piecePosition;
    public double angle;
}

class PositionsWrapper
{
    public string msgType;
    public CarWrapper[] data;
}

class TrackPieceWrapper
{
    public double length;
    public bool @switch;
    public double angle;
    public double radius;

    public bool IsAngle()
    {
        return angle != 0;
    }

    public double GetRealLength(double laneDistanceFromCenter)
    {
        if (length > 0)
            return length;
        else
            return 2 * Math.PI * GetRealRadius(laneDistanceFromCenter) * Math.Abs(angle) / 360;
    }

    public double GetRealRadius(double laneDistanceFromCenter)
    {
        return radius + laneDistanceFromCenter;
    }
}

class TrackLaneWrapper
{
    public double distanceFromCenter;
    public int index;
}

class TrackWrapper
{
    public TrackPieceWrapper[] pieces;
    public TrackLaneWrapper[] lanes;

    public double GetPieceRealLength(int pieceIndex, int laneIndex)
    {
        var piece = pieces[pieceIndex];
        return piece.GetRealLength(Math.Sign(piece.angle) * -lanes[laneIndex].distanceFromCenter);
    }

    public double GetPieceRealRadius(int pieceIndex, int laneIndex)
    {
        var piece = pieces[pieceIndex];
        return piece.GetRealRadius(Math.Sign(piece.angle) * -lanes[laneIndex].distanceFromCenter);
    }
}

class RaceWrapper
{
    public TrackWrapper track;
}

class GameWrapper
{
    public RaceWrapper race;
}


class RaceInitWrapper
{
    public string msgType;
    public GameWrapper data;
}


abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

